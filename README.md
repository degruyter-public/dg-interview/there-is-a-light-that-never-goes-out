Dear candidate,
Thanks for your interest on the role. Your performance on the interviews so far was impressive! We would like to move forward to the next step in which we would like to see your technical skills a little bit more detailed. That is an offline task with two parts. You can use any tecnology you want. When you finish please share the repository link with us so that we can check your codes. Note that in each task you will need to deliver a working code. Once we receive your code we will evaluate internally and reach out to you for an on-site (or online) meeting to discuss about your code, and how you approached the tasks.   


# Task
In this repository there is a simple web application. 
Application simply, displays a list of ISBNs and when you click on one ISBN it displays the title and the cover image of the ISBN. It also converts the numbers to romenic numbers. 
There is one JSON mockup file which provides all necessary information to the front end(books.json). Your deliverable application should be a working web application, which makes the request to the backend API that you will code not the local json file.

Your task will be change this mock up usage and write a proper backend for this application. The database layer or programming language/framework will be up to you, but still we will have some recommendations. 

Following tips would be differentiator for us: 
- If you use Scala/Play 
- if your code is self explanatory and well organized
- if you provide a good and comprehensive readme file. 
- if your code can easily be run and tested in local following the instructions in readme file.
- if you create a database backend (mongoDB, SQLLite, Postgres...etc up to your choice.) and fetch JSON from there. 


# Instructions to run the front-end 
You can **start the website** by cloning this repository to you local machine and follow these instructions:
  - Go to <code>dg-frontend-task-solution</code> folder on your machine
  - Run <code>npm install</code>
  - Run <code>npm run dev</code>
  - Open the dev server

You will hear from us about this task and we will invite you to a next step -in person if possible- expecting to discuss what you have delivered. 

Good luck and have fun! 

DG team.
